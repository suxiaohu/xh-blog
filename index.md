---
layout: home

title: 陋室铭
titleTemplate: 苔痕上阶绿，草色入帘青。谈笑有鸿儒，往来无白丁。

hero:
  name: "陋室铭"
  text: "斯是陋室，惟吾德馨。"
  tagline: 苔痕上阶绿，草色入帘青。谈笑有鸿儒，往来无白丁。
  image:
    src: /logo.png
    alt: Logo
  actions:
    - theme: brand
      text: 来看看
      link: /home
    - theme: alt
      text: 我是谁
      link: /about/about

features:
  - title: 介绍 me
    icon: ⚡️
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: 关于 me
    icon: 🖖
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: more me
    icon: 🛠️
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

